<?php
//  Website: eztemplate.co
//  E-Mail : eztemplate430@gmail.com

// Heading
$_['heading_title']       = 'حساب العميل';

// Text
$_['text_extension']      = 'الموديولات';
$_['text_success']        = 'تم التعديل!';
$_['text_edit']           = 'تحرير';

// Entry
$_['entry_status']        = 'الحالة';

// Error
$_['error_permission']    = 'تحذير: أنت لا تمتلك صلاحيات التعديل!';
