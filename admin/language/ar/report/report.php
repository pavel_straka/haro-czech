<?php
//  Website: eztemplate.co
//  E-Mail : eztemplate430@gmail.com

// Heading
$_['heading_title'] = 'التقارير';

// Text
$_['text_success']  = 'تم التعديل !';
$_['text_list']     = 'قائمة';
$_['text_type']     = 'اختر نوع التقرير';
$_['text_filter']   = 'فلتر';
