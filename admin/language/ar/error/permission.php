<?php
//  Website: www.eztemplate.co
//  E-Mail : eztemplate430@gmail.com

// Heading
$_['heading_title'] = 'صلاحيات محدودة !'; 

// Text
$_['text_permission'] = 'انت لا تمتلك صلاحية لدخول هذه الصفحة - الرجاء ابلاغ المدير.';