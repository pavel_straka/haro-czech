<?php
//  Website: www.eztemplate.co
//  E-Mail : eztemplate430@gmail.com

// Heading
$_['heading_title'] = 'الصفحة غير موجودة!';

// Text
$_['text_not_found'] = 'الصفحة التي طلبتها غير موجودة ! الرجاء ابلاغ المدير اذا تكررت هذه المشكلة.';
