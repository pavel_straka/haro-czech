/*! Authors & copyright (c) 2016: EZTemplate - Services(addonScript). */
$(document).ready(function() {

	$(window).load(function() { 
		$(".slideshow-panel .ezloading-bg").removeClass("ezloader");
	});
	$(window).load(function() 
	{ 
		$(".ezloading-bg").fadeOut("slow");
	})

	$( ".common-home .main-slider_inner" ).wrapAll( "<div class='main-slider'></div>" );

	$(".option-filter .list-group a").click(function() {
		$(this).toggleClass('collapsed').next('.list-group-item').slideToggle();
	});

	$("ul.breadcrumb li:nth-last-child(1) a").addClass('last-breadcrumb').removeAttr('href');

	$("#column-left .products-list .product-thumb, #column-right .products-list .product-thumb").unwrap();

	$("#content > h1, .account-wishlist #content > h2, .account-address #content > h2, .account-download #content > h2").first().addClass("page-title");

	$("#column-left .product-thumb .button-group .btn-cart,#column-right .product-thumb .button-group .btn-cart").removeAttr('data-original-title');

	$("ul.breadcrumb").insertAfter($("header"));
	$("#page > .breadcrumb").wrap("<div class='breadcrumb-wrapper'><div class='container'><div class='row'></div></div></div>");
	


	$('#column-left .product-thumb .image, #column-right .product-thumb .image').attr('class', 'image col-xs-5 col-sm-5 col-md-4');
	$('#column-left .product-thumb .thumb-description, #column-right .product-thumb .thumb-description').attr('class', 'thumb-description col-xs-7 col-sm-7 col-md-8');

	$( "body" ).append( "<div id='goToTop' title='Top' class='goToTop ttbox-img'><i class='fa fa-angle-double-up' aria-hidden='true'></i></div>" );
	$("#goToTop").hide();
	
	$('#content .row > .product-list .product-thumb .image').attr('class', 'image col-xs-5 col-sm-5 col-md-4');
	$('#content .row > .product-list .product-thumb .thumb-description').attr('class', 'thumb-description col-xs-7 col-sm-7 col-md-8');
	$('#content .row > .product-grid .product-thumb .image').attr('class', 'image col-xs-12');
	$('#content .row > .product-grid .product-thumb .thumb-description').attr('class', 'thumb-description col-xs-12');

	$('select.form-control').wrap("<div class='select-wrapper'></div>");
	$('input[type="checkbox"]').wrap("<span class='checkbox-wrapper'></span>");
	$('input[type="checkbox"]').attr('class','checkboxid');
	$('input[type="radio"]').wrap("<span class='radio-wrapper'></span>");
	$('input[type="radio"]').attr('class','radioid');
	
	/*---------------------- Inputtype Js Start -----------------------------*/
	$('.checkboxid').change(function(){
		if($(this).is(":checked")) {
			$(this).addClass("chkactive");
			$(this).parent().addClass('active');
		} else {
			$(this).removeClass("chkactive");
			$(this).parent().removeClass('active');
		}
	});

	$(function() {
		var $radioButtons = $('input[type="radio"]');
		$radioButtons.click(function() {
			$radioButtons.each(function() {
				$(this).parent().toggleClass('active', this.checked);
			});
		});
	});
	/*---------------------- Inputtype Js End -----------------------------*/
	/* Special js */
	var ezspecialproduct = $(".ez-special-block .products-carousel");
	ezspecialproduct.owlCarousel({
		items:5,
		itemsDesktop : [1249,4],
		itemsDesktopSmall : [991,3],
		itemsTablet: [767,2],
		itemsMobile : [480,1],
		navigation: true,
		pagination: false
	});

	/* Testimonial js */
	var eztestimonial = $("#eztestimonial-carousel");
	eztestimonial.owlCarousel({
		autoPlay : false,
		paginationNumbers : true,
     	 items : 1, //10 items above 1000px browser width
     	 itemsDesktop : [1200,1], 
     	 itemsDesktopSmall : [991,1], 
     	 itemsTablet: [767,1], 
     	 itemsMobile : [480,1],
     	 navigation : true
     	});

      // Custom Navigation Events
      $(".eztestimonial_next").click(function(){
      	eztestimonial.trigger('owl.next');
      })

      $(".eztestimonial_prev").click(function(){
      	eztestimonial.trigger('owl.prev');
      })

      /* ----------- SmartBlog Js Start ----------- */
      var ezblog = $("#ezsmartblog-carousel");
      ezblog.owlCarousel({
     	 items :3, //10 items above 1000px browser width
     	 itemsDesktop : [1249,3], 
     	 itemsDesktopSmall : [991,2], 
     	 itemsTablet: [767,2], 
     	 itemsMobile : [543,1],
     	 navigation: true,
     	 pagination: false
     	});

      /* ----------- additional Js Start ----------- */
      $(".additional-images").owlCarousel({
      	items: 4,
      	itemsDesktop: [1249,3],
      	itemsDesktopSmall: [991,2],
      	itemsTablet: [767,3],
      	itemsMobile: [480,2],
      	autoPlay: false,
      	navigation: true,
      	pagination: false
      });

// Custom Navigation Events
$(".additional-next").click(function(){
	$(".additional-images").trigger('owl.next');
})
$(".additional-prev").click(function(){
	$(".additional-images").trigger('owl.prev');
})
$(".additional-images-container .customNavigation").addClass('owl-navigation');

/* ----------- additional Js End ----------- */


// Carousel Counter
colsCarousel = $('#column-right, #column-left').length;
if (colsCarousel == 2) {
	ci=3;
} else if (colsCarousel == 1) {
	ci=4;
} else {
	ci=5;
}

// product Carousel
$("#content .products-carousel").owlCarousel({
	items: ci,
	itemsDesktop : [1249,4], 
	itemsDesktopSmall : [991,3], 
	itemsTablet: [543,2], 
	itemsMobile : [480,1], 
	navigation: true,
	pagination: false
});

$(".customNavigation .next").click(function(){
	$(this).parent().parent().find(".products-carousel").trigger('owl.next');
})
$(".customNavigation .prev").click(function(){
	$(this).parent().parent().find(".products-carousel").trigger('owl.prev');
})
$(".products-list .customNavigation").addClass('owl-navigation');


/* Go to Top JS START */
$(function () {
	$(window).scroll(function () {
		if ($(this).scrollTop() > 150) {
			$('.goToTop').fadeIn();
		} else {
			$('.goToTop').fadeOut();
		}
	});
	
		// scroll body to 0px on click
		$('.goToTop').click(function () {
			$('body,html').animate({
				scrollTop: 0
			}, 1000);
			return false;
		});
	});
/* Go to Top JS END */

$(".product-category .product-list .product-thumb .image .btn-merge").each(function(){
	$(this).appendTo($(this).parent().parent().find(".thumb-description .button-group"));
});
$(".product-category .product-grid .product-thumb .thumb-description .button-group .btn-merge").each(function(){
	$(this).appendTo($(this).parent().parent().parent().find(".image"));
});


/* Active class in Product List Grid START */
$('#list-view').click(function() {
	$('#grid-view').removeClass('active');
	$('#list-view').addClass('active');
	$('#content .row > .product-list .product-thumb .image').attr('class', 'image col-xs-5 col-sm-5 col-md-4');
	$('#content .row > .product-list .product-thumb .thumb-description').attr('class', 'thumb-description col-xs-7 col-sm-7 col-md-8');
	$(".product-category .product-list .product-thumb .image .btn-merge").each(function(){
		$(this).appendTo($(this).parent().parent().find(".thumb-description .button-group"));
	});

});
$('#grid-view').click(function() {
	$('#list-view').removeClass('active');
	$('#grid-view').addClass('active');
	$('#content .row > .product-grid .product-thumb .image').attr('class', 'image col-xs-12');
	$('#content .row > .product-grid .product-thumb .thumb-description').attr('class', 'thumb-description col-xs-12');
	$(".product-category .product-grid .product-thumb .thumb-description .button-group .btn-merge").each(function(){
		$(this).appendTo($(this).parent().parent().parent().find(".image"));
	});
});

if (localStorage.getItem('display') == 'list') {
	$('#list-view').addClass('active');
} else {
	$('#grid-view').addClass('active');
}
/* Active class in Product List Grid END */
});
// Documnet.ready() over....

function cartToggle() {
	if($( window ).width() >= 992) {
		$("#cart button.dropdown-toggle").unbind("click");
		$("#cart button.dropdown-toggle").click(function(){
			$( ".header-cart-toggle" ).slideToggle( "2000" );														 
			$(".account-link-toggle").slideUp("slow");
			$(".language-toggle").slideUp("slow");
			$(".currency-toggle").slideUp("slow");
		});
	}
	else {
		$("#cart button.dropdown-toggle").unbind("click");
		$("#cart button.dropdown-toggle").click(function(){
			$( ".header-cart-toggle" ).slideToggle( "2000" );														 
			$(".account-link-toggle").slideUp("slow");
			$(".language-toggle").slideUp("slow");
			$(".currency-toggle").slideUp("slow");
		});	
	}
}
$(document).ready(function() {cartToggle();});
$(window).resize(function() {cartToggle();});


function currencyToggle() {
	if($( window ).width() >= 992) {
		$("#form-currency button.dropdown-toggle").unbind("click");
		$("#form-currency button.dropdown-toggle").click(function(){									 
			$( ".currency-toggle" ).slideToggle( "2000" );	
			$(".language-toggle").slideUp("slow");
			$(".account-link-toggle").slideUp("slow");
			$(".header-cart-toggle").slideUp("slow");

		});
	}
	else {
		$("#form-currency button.dropdown-toggle").unbind("click");
		$("#form-currency button.dropdown-toggle").click(function(){
			$( ".currency-toggle" ).slideToggle( "2000" );	
			$(".language-toggle").slideUp("slow");
			$(".account-link-toggle").slideUp("slow");
			$(".header-cart-toggle").slideUp("slow");
		});
	}
}
$(document).ready(function() {currencyToggle();});
$(window).resize(function() {currencyToggle();});

function languageToggle() {
	if($( window ).width() >= 992) {
		$("#form-language button.dropdown-toggle").unbind("click");
		$("#form-language button.dropdown-toggle").click(function(){
			$( ".language-toggle" ).slideToggle( "2000" );
			$(".currency-toggle").slideUp("slow");
			$(".account-link-toggle").slideUp("slow");
			$(".header-cart-toggle").slideUp("slow");
		});
	}
	else {
		$("#form-language button.dropdown-toggle").unbind("click");
		$("#form-language button.dropdown-toggle").click(function(){
			$( ".language-toggle" ).slideToggle( "2000" );																  
			$(".currency-toggle").slideUp("slow");
			$(".account-link-toggle").slideUp("slow");
			$(".header-cart-toggle").slideUp("slow");
		});
	}
}
$(document).ready(function() {languageToggle();});
$(window).resize(function() {languageToggle();});

function accountToggle() {
	if($( window ).width() >= 992) {
		$("#top a.dropdown-toggle").unbind("click");
		$("#top a.dropdown-toggle").click(function(){
			$( ".account-link-toggle" ).slideToggle( "2000" );
			if ($("#form-currency")[0]){
				$(".currency-toggle").css('display', 'none');
			}
			if ($("#form-language")[0]){
				$(".language-toggle").css('display', 'none');
			}
			$(".header-cart-toggle").slideUp("slow");
		});
	}
	else {
		$("#top a.dropdown-toggle").unbind("click");
		$("#top a.dropdown-toggle").click(function(){
			$( ".account-link-toggle" ).slideToggle( "2000" );
			if ($("#form-currency")[0]){
				$(".currency-toggle").css('display', 'none');
			}
			if ($("#form-language")[0]){
				$(".language-toggle").css('display', 'none');
			}
			$(".header-cart-toggle").slideUp("slow");
		});
	}
}
$(document).ready(function() {accountToggle();});
$(window).resize(function() {accountToggle();});


function footerToggle() {
	if($( window ).width() < 992) {
		$("footer .mobile_toggle").remove();
		$("footer .footer-column h5").append( "<a class='mobile_toggle'>&nbsp;</a>" );
		$("footer .footer-column h5").addClass( "toggle" );
		$("footer .mobile_toggle").click(function() {
			$(this).parent().toggleClass('active').parent().find('ul').slideToggle('slow');
		});
	} else {
		$("footer .footer-column h5").parent().find('ul').removeAttr('style');
		$("footer .footer-column h5").removeClass('toggle');
		$("footer .footer-column h5").removeClass('active');
		$("footer .mobile_toggle").remove();
	}
}
$(document).ready(function() {footerToggle();});
$(window).resize(function() {footerToggle();});



/* Category List - Tree View */
function categoryListTreeView() {
	$(".category-treeview li.category-li").find("ul").parent().prepend("<div class='list-tree'></div>").find("ul").css('display','none');

	$(".category-treeview li.category-li.category-active").find("ul").css('display','block');
	$(".category-treeview li.category-li.category-active").toggleClass('active');
}
$(document).ready(function() {categoryListTreeView();});


/* Category List - TreeView Toggle */
function categoryListTreeViewToggle() {
	$(".category-treeview li.category-li .list-tree").click(function() {
		$(this).parent().toggleClass('active').find('ul').slideToggle();
	});
}
$(document).ready(function() {categoryListTreeViewToggle();});
$(document).ready(function(){ menuMore(); });
/* Main Menu - MORE items */

function menuMore(){
	var max_items = 5;
	var liItems = $('.navbar-nav > li');
	var remainItems = liItems.slice(max_items, liItems.length);
	remainItems.wrapAll('<li class="dropdown more-menu"><div class="dropdown-menu"><div class="dropdown-inner"><ul class="list-unstyled childs_1">');
	$('.more-menu').prepend('<span>More</span>');
}
function menuToggle() {
	if($( window ).width() < 992) {
		$("#menu .navbar-collapse ul  li.dropdown > i").remove(".fa.fa-angle-down");
		$("#menu .navbar-collapse ul  li.dropdown > a").after("<i class='fa fa-angle-down'></i>");
		$("#menu .navbar-collapse ul  li.dropdown.more-menu > i").remove(".fa.fa-angle-down");
		$("#menu .navbar-collapse ul li.dropdown.more-menu > span").after("<i class='fa fa-angle-down'></i>");
	} else {
		$("#menu .navbar-collapse ul li.dropdown > i").remove(".fa.fa-angle-down");
	}

	/* menu item toggle active */
	$("#menu .navbar-collapse ul li.dropdown > i").click(function() {
		$(this).parent().toggleClass('active').find(".dropdown-menu").first().slideToggle();
	});
}
$(document).ready(function() {menuToggle();});
$( window ).resize(function(){menuToggle();});


/* Animate effect on Review Links - Product Page */
$(".product-total-review, .product-write-review").click(function() {
	$('html, body').animate({ scrollTop: $(".product-tabs").offset().top }, 1000);
});
// header Fixed  
function ezheaderfixed() {
 	  if ($(this).scrollTop() > 230) {
            $("header .header-bottom").addClass('fixed'); 
            $(".ezcart").insertAfter('#menu');

        }else{
            $("header .header-bottom").removeClass('fixed');
            $(".ezcart").prependTo('.header-right');
        }
  }
jQuery(window).resize(function() {ezheaderfixed();});
jQuery(document).ready(function() {ezheaderfixed();});
jQuery(window).scroll(function() {ezheaderfixed();});

/*category filter js*/
function columnToggle() {
	if($( window ).width() < 992) {
		$('#column-left,#column-right').insertAfter('#content');
		$("#column-left .panel-heading, #column-right .panel-heading").addClass( "toggle" );
		$("#column-left .list-group, #column-right .list-group").css( 'display', 'none' );
		$("#column-left .panel-default.active .list-group, #column-right .panel-default.active .list-group").css( 'display', 'block' );
		$("#column-left .panel-heading.toggle, #column-right .panel-heading.toggle").unbind("click");
		$("#column-left .panel-heading.toggle, #column-right .panel-heading.toggle").click(function() {
			$(this).parent().toggleClass('active').find('.list-group').slideToggle( "slow" );
		});

		$("#column-left .box-heading, #column-right .box-heading").addClass( "toggle" );
		$("#column-left .products-content, #column-right .products-content").css( 'display', 'none' );
		$("#column-left .products-list.active .products-content, #column-right .products-list.active .products-content").css( 'display', 'block' );
		$("#column-left .box-heading.toggle, #column-right .box-heading.toggle").unbind("click");
		$("#column-left .box-heading.toggle, #column-right .box-heading.toggle").click(function() {
			$(this).parent().toggleClass('active').find('.products-content').slideToggle( "slow" );
		});

	} else {

		$('#column-left').insertBefore('#content');
		$('#column-right').insertAfter('#content');
		$("#column-left .panel-heading, #column-right .panel-heading").unbind("click");
		$("#column-left .panel-heading, #column-right .panel-heading").removeClass( "toggle" );
		$("#column-left .list-group, #column-right .list-group").css( 'display', 'block' );

		$("#column-left .box-heading, #column-right .box-heading").unbind("click");
		$("#column-left .box-heading, #column-right .box-heading").removeClass( "toggle" );
		$("#column-left .products-content, #column-right .products-content").css( 'display', 'block' );

	}
}

$(document).ready(function() {columnToggle();});
$(window).resize(function() {columnToggle();});


function responsivecolumn()
{
	if ($(document).width() <= 991)
	{
		$('#page > .container > .row > #column-left').appendTo('#page  > .container > .row');
	}
	else if($(document).width() >= 992)
	{
		$('#page > .container > .row > #column-left').prependTo('#page  > .container > .row');
	}
}
$(document).ready(function(){responsivecolumn();});
$(window).resize(function(){responsivecolumn();});
/*category filter js end*/


$(document).ready(function(){

	function productZoom(){
			$('.product-images .product-image img').elevateZoom({
				zoomType: "lens",
				cursor: "crosshair",
				lensShape : "square",
				lensSize    : 200,
				galleryActiveClass: "active",
				zoomWindowFadeIn: 500,
				zoomWindowFadeOut: 500
			});

			$('body').on('mouseenter','.product-images .product-image img', function(){
				$('.zoomContainer').remove();
				$(this).removeData('elevateZoom');
				$(this).attr('src', $(this).attr('src'));
				$(this).data('zoom-image', $(this).data('zoom-image'));
				$(this).elevateZoom({zoomType:"lens",cursor: "crosshair",lensShape : "square",lensSize    : 200});
			});
			$('body').on('click',('.js-qv-product-images .js-thumb'),function(e){
				e.preventDefault();
				var zoom_val = $(this).attr('src');
				/*$(".zoomLens img").attr('src', $(this).attr('data-image-large-src'));*/
				$(".zoomLens").css("background-image","url("+ $(this).attr('data-image-large-src') +")");
				$('.zoomWindowContainer div').css("background-image","url("+ $(this).attr('data-image-large-src') +")");
			});
		}
	$(document).ready(function(){ productZoom();});
	$(window).resize(function(){ productZoom();});

});

setTimeout(function() {
	$('body .alert.alert-success').fadeOut('slow');
}, 4000);