<?php
// Heading 
$_['heading_title'] = 'Our Products';

$_['tab_latest'] = 'Latest Products';
$_['tab_bestseller'] = 'Bestseller Products';
$_['tab_featured'] = 'Featured Products';
$_['tab_special'] = 'Special Products';

// Text
$_['button_wishlist'] = 'Add to wishlist';
$_['button_compare'] = 'Add to Compare';
$_['button_cart'] = 'Add to Cart';
$_['text_tax'] = 'Without tax:';
?>