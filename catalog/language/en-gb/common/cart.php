<?php
// Text
$_['text_shopping']  = 'My Cart';
$_['text_items']     = '<span class="cart-count">%s</span><span class="cart-total">%s</span>';
$_['text_empty']     = 'Your shopping cart is empty!';
$_['text_cart']      = 'View Cart';
$_['text_checkout']  = 'Checkout';
$_['text_recurring'] = 'Payment Profile';