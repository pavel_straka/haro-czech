<?php
//  Website: eztemplate.co
//  E-Mail : eztemplate430@gmail.com

// Text
$_['text_items']    = '<span class="cart-count">%s</span><span class="cart-total">%s</span>';
$_['text_empty']    = 'سلة الشراء فارغة !';
$_['text_cart']     = 'معاينة السلة';
$_['text_checkout'] = 'إنهاء الطلب';
$_['text_recurring']  = 'ملف الدفع';