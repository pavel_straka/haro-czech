<?php
//  Website: eztemplate.co
//  E-Mail : eztemplate430@gmail.com

// Text
$_['text_success']     = 'تم القبول !';

// Error
$_['error_permission'] = 'تحذير: انت لا تمتلك صلاحيات الدخول الى واجهة برمجة - API!';
$_['error_coupon']     = 'تحذير: الكوبون غير صالح او قد استخدامه او انتهت صلاحيته !';