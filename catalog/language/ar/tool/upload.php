<?php
//  Website: eztemplate.co
//  E-Mail : eztemplate430@gmail.com

// Text
$_['text_upload']    = 'تم رفع الملف بنجاح !';

// Error
$_['error_filename'] = 'اسم الملف يجب ان يكون بين  3 و 64 رمز !';
$_['error_filetype'] = 'نوع الملف غير مدعوم!';
$_['error_upload']   = 'مطلوب !';
