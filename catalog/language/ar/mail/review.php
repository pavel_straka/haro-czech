<?php
//  Website: eztemplate.co
//  E-Mail : eztemplate430@gmail.com

// Text
$_['text_subject']	= '%s - تقييم المنتج';
$_['text_waiting']	= 'يوجد تقييم جديد في قائمة الانتظار.';
$_['text_product']	= 'المنتج: %s';
$_['text_reviewer']	= 'تم التقييم بواسطة العميل: %s';
$_['text_rating']	= 'التقييم: %s';
$_['text_review']	= 'اتعليق:';